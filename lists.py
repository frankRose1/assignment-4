myUniqueList = []
myLeftovers = []


def add_to_unique_list(new_value):
    if len(myUniqueList) >= 1:
        for value in myUniqueList:
            if new_value in myUniqueList:
                myLeftovers.append(new_value)
                return False
            else:
                myUniqueList.append(new_value)
                return True
    else:
        myUniqueList.append(new_value)
        return True


# unique values
add_to_unique_list(5)
add_to_unique_list(3.3)
add_to_unique_list('3.3')
add_to_unique_list('Hello')
# duplicate values
add_to_unique_list('World')
add_to_unique_list('World')
add_to_unique_list(2)
add_to_unique_list(2)

print('Printing unique list')
print(myUniqueList)  # [5, 3.3, '3.3', 'Hello', 'World', 2]
print('-'*20)
print('Printing leftover list')
print(myLeftovers)  # ['World', 2]
